/**
 * Created by vladimirkavlakan on 11/11/2016.
 */
const express = require("express");
const app = express();
const cors = require("cors");

app.use(cors());
app.get("/", function(req, res, next) {
    console.log(req.query);
    let a = req.query.a;
    a = !a || isNaN(Number(a)) ? 0 : Number(a);
    var b = req.query.b;
    b = !b || isNaN(Number(b)) ? 0 : Number(b);

    res.send(`${a + b}`);
});

app.listen(8080);
